import 'package:flutter/material.dart';
import 'package:prueba_pragma/modelos/gato.dart';

class Gatos extends ChangeNotifier {
  final List<Gato> items = [];

  void agregar(Gato item) {
    items.add(item);
    notifyListeners();
  }

  void limpiar() {
    items.clear();
    notifyListeners();
  }

  void actualizar() {
    notifyListeners();
  }
}

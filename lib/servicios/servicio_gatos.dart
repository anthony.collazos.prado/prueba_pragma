import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:prueba_pragma/generales/general_url.dart';
import 'package:prueba_pragma/modelos/gato.dart';
import 'package:prueba_pragma/modelos/gatos.dart';

class ServicioGatos {
  Future<void> obtenerDatos(BuildContext context) async {
    final proveedor = Provider.of<Gatos>(context, listen: false);
    final url = Uri.parse(GeneralUrl.url);
    final respuesta = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': 'bda53789-d59e-46cd-9bc4-2936630fde39'
      },
    );

    List<dynamic> resultado = jsonDecode(respuesta.body);
    proveedor.limpiar();
    for (var i = 0; i < resultado.length; i++) {
      Gato gato = Gato();
      if (resultado[i]['weight'] != null) {
        Map<String, String> weight = {
          'imperial': resultado[i]['weight']['imperial'],
          'metric': resultado[i]['weight']['metric'],
        };
        gato.agregarWeight(weight);
      }
      gato.name = resultado[i]['name'];
      gato.cfaUrl = resultado[i]['cfa_url'];
      gato.vetstreetUrl = resultado[i]['vetstreet_url'];
      gato.vcahospitalsUrl = resultado[i]['vcahospitals_url'];
      gato.temperament = resultado[i]['temperament'];
      gato.origin = resultado[i]['origin'];
      gato.countryCodes = resultado[i]['country_codes'];
      gato.countryCode = resultado[i]['country_code'];
      gato.description = resultado[i]['description'];
      gato.lifeSpan = resultado[i]['life_span'];
      gato.indoor = resultado[i]['indoor'];
      gato.lap = resultado[i]['lap'];
      gato.altNames = resultado[i]['alt_names'];
      gato.adaptability = resultado[i]['adaptability'];
      gato.affectionLevel = resultado[i]['affection_level'];
      gato.childFriendly = resultado[i]['child_friendly'];
      gato.dogFriendly = resultado[i]['dog_friendly'];
      gato.energyLevel = resultado[i]['energy_level'];
      gato.grooming = resultado[i]['grooming'];
      gato.healthIssues = resultado[i]['health_issues'];
      gato.intelligence = resultado[i]['intelligence'];
      gato.sheddingLevel = resultado[i]['shedding_level'];
      gato.socialNeeds = resultado[i]['social_needs'];
      gato.strangerFriendly = resultado[i]['stranger_friendly'];
      gato.vocalisation = resultado[i]['vocalisation'];
      gato.experimental = resultado[i]['experimental'];
      gato.hairless = resultado[i]['hairless'];
      gato.natural = resultado[i]['natural'];
      gato.rare = resultado[i]['rare'];
      gato.rex = resultado[i]['rex'];
      gato.suppressedTail = resultado[i]['suppressed_tail'];
      gato.shortLegs = resultado[i]['short_legs'];
      gato.wikipediaUrl = resultado[i]['wikipedia_url'];
      gato.hypoallergenic = resultado[i]['hypoallergenic'];
      gato.referenceImageId = resultado[i]['reference_image_id'];
      if (resultado[i]['image'] != null) {
        Map<String, dynamic> image = {
          'id': resultado[i]['image']['id'],
          'width': resultado[i]['image']['width'],
          'height': resultado[i]['image']['height'],
          'url': resultado[i]['image']['url'],
        };
        gato.agregarImage(image);
      }
      proveedor.agregar(gato);
    }
  }
}

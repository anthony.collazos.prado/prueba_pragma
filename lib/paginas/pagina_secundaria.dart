import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_pragma/generales/general_elemento.dart';
import 'package:prueba_pragma/generales/general_estilo.dart';
import 'package:prueba_pragma/modelos/gatos.dart';

class PaginaSecundaria extends StatefulWidget {
  const PaginaSecundaria({Key? key}) : super(key: key);

  @override
  PaginaSecundariaState createState() => PaginaSecundariaState();
}

class PaginaSecundariaState extends State<PaginaSecundaria> {
  @override
  Widget build(BuildContext context) {
    int? argumento = ModalRoute.of(context)!.settings.arguments as int;
    final proveedor = Provider.of<Gatos>(context, listen: false);
    final pantalla = MediaQuery.of(context).size;

    return Scaffold(
      appBar: mostrarCabecera(context),
      backgroundColor: Colors.grey.shade200,
      body: SafeArea(
        child: Container(
          constraints: const BoxConstraints.expand(),
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: obtenerMarco(pantalla)),
            child: Column(
              children: _crearContenido(
                  proveedor, int.parse(argumento.toString()), pantalla),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _crearContenido(Gatos proveedor, int argumento, Size pantalla) {
    final objeto = proveedor.items[argumento];

    List<Widget> contenido = [
      GeneralEstilo.espaciadorX3,
      Row(
        children: <Widget>[
          const Icon(Icons.pets, color: Colors.blue),
          GeneralEstilo.espaciadorY1,
          Text('Gato ${objeto.name}', style: GeneralEstilo.h1Azul),
        ],
      ),
      GeneralEstilo.espaciadorX3,
      Container(
        width: obtenerAncho(pantalla),
        padding: GeneralEstilo.paddingSegundo,
        decoration: GeneralEstilo.cajaBlanca,
        child: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: 300.0,
              child: Image.network(
                objeto.image['url'],
                fit: BoxFit.cover,
              ),
            ),
            GeneralEstilo.espaciadorX2,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: RichText(
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: '${objeto.description}\n\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Temperamento: ${objeto.temperament}\n\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'País de origen: ${objeto.origin}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Inteligencia: ${objeto.intelligence}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Adaptabilidad: ${objeto.adaptability}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Problemas de salud: ${objeto.healthIssues}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Amigo de los perros: ${objeto.dogFriendly}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Tiempo de vida: ${objeto.lifeSpan}',
                          style: GeneralEstilo.pGris,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ];
    contenido.add(GeneralEstilo.espaciadorX3);

    return contenido;
  }
}

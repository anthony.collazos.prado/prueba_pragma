import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_pragma/generales/general_elemento.dart';
import 'package:prueba_pragma/generales/general_estilo.dart';
import 'package:prueba_pragma/modelos/gatos.dart';
import 'package:prueba_pragma/servicios/servicio_gatos.dart';

class PaginaPrincipal extends StatefulWidget {
  const PaginaPrincipal({Key? key}) : super(key: key);

  @override
  PaginaPrincipalState createState() => PaginaPrincipalState();
}

class PaginaPrincipalState extends State<PaginaPrincipal> {
  bool paginaCargada = false;

  int paginaActual = 1;
  int paginaActualBotones = 1;
  ServicioGatos servicioGatos = ServicioGatos();

  @override
  Widget build(BuildContext context) {
    Widget objeto;
    final pantalla = MediaQuery.of(context).size;

    if (paginaCargada == false) {
      objeto = FutureBuilder(
        future: servicioGatos.obtenerDatos(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return pantallaCargando();
          } else {
            return Scaffold(
              appBar: mostrarCabecera(context),
              backgroundColor: Colors.grey.shade200,
              body: SafeArea(
                child: Container(
                  constraints: const BoxConstraints.expand(),
                  child: SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                      horizontal: obtenerMarco(pantalla),
                    ),
                    child: Column(
                      children: _crearContenido(pantalla),
                    ),
                  ),
                ),
              ),
            );
          }
        },
      );
    } else {
      objeto = Scaffold(
        appBar: mostrarCabecera(context),
        backgroundColor: Colors.grey.shade200,
        body: SafeArea(
          child: Container(
            constraints: const BoxConstraints.expand(),
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(
                horizontal: obtenerMarco(pantalla),
              ),
              child: Column(
                children: _crearContenido(pantalla),
              ),
            ),
          ),
        ),
      );
    }

    setState(() => paginaCargada = true);

    return objeto;
  }

  List<Widget> _crearContenido(Size pantalla) {
    Gatos proveedor = Provider.of<Gatos>(context, listen: false);
    int porPagina = 5;
    int longitud = proveedor.items.length;
    int cantidadPaginas = _obtenerPaginas(longitud, porPagina);
    List<int> limites =
        _obtenerLimites(longitud, paginaActual, cantidadPaginas, porPagina);

    List<Widget> contenido = [
      GeneralEstilo.espaciadorX2,
      Row(
        children: <Widget>[
          const Icon(Icons.list_alt, color: Colors.blue),
          GeneralEstilo.espaciadorY1,
          const Text(' Listado de gatos', style: GeneralEstilo.h1Azul),
          const Spacer(),
          _botonAtras(),
          _botonAdelante(cantidadPaginas),
        ],
      ),
      GeneralEstilo.espaciadorX2,
      Container(
        width: obtenerAncho(pantalla),
        padding: GeneralEstilo.paddingTercero,
        decoration: GeneralEstilo.cajaBlanca,
        child: TextFormField(
          keyboardType: TextInputType.text,
          decoration: const InputDecoration(
            icon: Icon(Icons.search),
            labelText: 'Búsqueda',
            border: InputBorder.none,
          ),
          onFieldSubmitted: (String valor) {
            _buscar(valor);
          },
        ),
      ),
      GeneralEstilo.espaciadorX1,
    ];

    for (var i = limites[0]; i < limites[1]; i++) {
      final objeto = proveedor.items[i];

      contenido.add(GeneralEstilo.espaciadorX1);
      contenido.add(
        ElevatedButton(
          style: GeneralEstilo.botonBlanco,
          child: Container(
            width: obtenerAncho(pantalla),
            padding: GeneralEstilo.paddingPrimero,
            child: Row(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 100.0,
                      height: 100.0,
                      child: Image.network(
                        objeto.image['url'],
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
                GeneralEstilo.espaciadorY1,
                Expanded(
                  child: RichText(
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Raza: ${objeto.name}\n',
                          style: GeneralEstilo.h2Azul,
                        ),
                        TextSpan(
                          text: 'País de origen: ${objeto.origin}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Inteligencia: ${objeto.intelligence}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Problemas de salud: ${objeto.healthIssues}\n',
                          style: GeneralEstilo.pGris,
                        ),
                        TextSpan(
                          text: 'Amigo de los perros: ${objeto.dogFriendly}',
                          style: GeneralEstilo.pGris,
                        ),
                      ],
                    ),
                  ),
                ),
                GeneralEstilo.espaciadorY1,
                Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.blue.shade900,
                ),
              ],
            ),
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'secundaria', arguments: i);
          },
        ),
      );
    }
    contenido.add(GeneralEstilo.espaciadorX2);
    contenido.add(Wrap(children: _botonesPaginas(cantidadPaginas)));
    contenido.add(GeneralEstilo.espaciadorX2);

    return contenido;
  }

  void _buscar(String valor) {
    Gatos proveedor = Provider.of<Gatos>(context, listen: false);

    for (var i = 0; i < proveedor.items.length; i++) {
      if (valor.toLowerCase() == proveedor.items[i].name!.toLowerCase()) {
      } else {
        proveedor.items.removeAt(i);
        proveedor.actualizar();
      }
    }

    if (valor == '') {
      setState(() => paginaCargada = false);
    } else {
      setState(() => paginaCargada = true);
    }
  }

  int _obtenerPaginas(int longitud, int porPagina) {
    double resultado = longitud / porPagina;
    int cantidadPaginas = resultado.ceil();
    return cantidadPaginas;
  }

  List<int> _obtenerLimites(
      int longitud, int numeroPagina, int cantidadPaginas, int porPagina) {
    int maximo;
    int modulo = longitud % porPagina;
    int limiteInferior = numeroPagina * porPagina - porPagina;
    int limiteSuperior = numeroPagina * porPagina;
    (modulo == 0)
        ? maximo = cantidadPaginas * porPagina
        : maximo = cantidadPaginas * porPagina - porPagina;
    if (limiteSuperior > maximo && modulo > 0) {
      limiteSuperior = limiteInferior + modulo;
    }
    List<int> resultado = [limiteInferior, limiteSuperior];
    return resultado;
  }

  Widget _botonAtras() {
    Widget boton;
    if (paginaActual > 1) {
      boton = IconButton(
        icon: const Icon(Icons.keyboard_arrow_left, color: Colors.blue),
        padding: EdgeInsets.zero,
        onPressed: () => setState(() => paginaActual--),
      );
    } else {
      boton = const SizedBox(width: 0.0);
    }
    return boton;
  }

  Widget _botonAdelante(int cantidadPaginas) {
    Widget boton;
    if (paginaActual < cantidadPaginas) {
      boton = IconButton(
        icon: const Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        padding: EdgeInsets.zero,
        onPressed: () => setState(() => paginaActual++),
      );
    } else {
      boton = const SizedBox(width: 0.0);
    }
    return boton;
  }

  List<Widget> _botonesPaginas(int cantidadPaginas) {
    List<Widget> botones = [];
    int porPagina = 5;
    int paginasBotones = _obtenerPaginas(cantidadPaginas, porPagina);
    List<int> limites = _obtenerLimites(
        cantidadPaginas, paginaActualBotones, paginasBotones, porPagina);

    if (paginaActualBotones > 1) {
      botones.add(
        IconButton(
          icon: const Icon(Icons.keyboard_arrow_left, color: Colors.blue),
          padding: EdgeInsets.zero,
          onPressed: () => setState(() => paginaActualBotones--),
        ),
      );
    }
    for (var i = limites[0] + 1; i <= limites[1]; i++) {
      botones.add(
        IconButton(
          icon: (paginaActual == i)
              ? Container(
                  decoration: GeneralEstilo.circuloAzul,
                  child: Center(
                    child: Text('$i', style: GeneralEstilo.tBlancoBoton),
                  ),
                )
              : Text('$i', style: GeneralEstilo.tAzulBoton),
          onPressed: () => setState(() => paginaActual = i),
        ),
      );
    }
    if (paginaActualBotones < paginasBotones) {
      botones.add(
        IconButton(
          icon: const Icon(Icons.keyboard_arrow_right, color: Colors.blue),
          padding: EdgeInsets.zero,
          onPressed: () => setState(() => paginaActualBotones++),
        ),
      );
    }

    return botones;
  }
}

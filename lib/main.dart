import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_pragma/modelos/gatos.dart';
import 'package:prueba_pragma/paginas/pagina_principal.dart';
import 'package:prueba_pragma/paginas/pagina_secundaria.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Gatos()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Prueba Pragma',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: 'principal',
      routes: {
        'principal': (BuildContext context) => const PaginaPrincipal(),
        'secundaria': (BuildContext context) => const PaginaSecundaria(),
      },
    );
  }
}

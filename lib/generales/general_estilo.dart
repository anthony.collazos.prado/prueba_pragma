import 'package:flutter/material.dart';

class GeneralEstilo {
  static const h1Azul = TextStyle(
    fontSize: 22.0,
    color: Colors.blue,
  );
  static final h2Azul = TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
    color: Colors.blue.shade900,
  );
  static const pGris = TextStyle(
    fontSize: 14.0,
    color: Colors.black54,
  );
  static const tAzulBoton = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    color: Colors.blue,
  );
  static const tBlancoBoton = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );
  static const espaciadorX1 = SizedBox(height: 10.0);
  static const espaciadorX2 = SizedBox(height: 20.0);
  static const espaciadorX3 = SizedBox(height: 30.0);
  static const espaciadorY1 = SizedBox(width: 10.0);
  static const espaciadorY2 = SizedBox(width: 20.0);
  static const espaciadorY3 = SizedBox(width: 30.0);
  static const paddingPrimero = EdgeInsets.only(
    top: 15.0,
    right: 10.0,
    bottom: 15.0,
    left: 15.0,
  );
  static const paddingSegundo = EdgeInsets.all(15.0);
  static const paddingTercero = EdgeInsets.only(right: 15.0, left: 15.0);
  static const cajaBlanca = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
    boxShadow: <BoxShadow>[
      BoxShadow(
          color: Colors.black12, offset: Offset(0.0, 3.0), blurRadius: 2.0),
    ],
  );
  static final ButtonStyle botonBlanco = ElevatedButton.styleFrom(
    primary: Colors.white,
    padding: EdgeInsets.zero,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
  );
  static const circuloAzul = BoxDecoration(
    color: Colors.blue,
    shape: BoxShape.circle,
  );
}

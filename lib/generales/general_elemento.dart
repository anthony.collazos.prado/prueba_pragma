import 'package:flutter/material.dart';
import 'package:prueba_pragma/generales/general_imagen.dart';

AppBar mostrarCabecera(BuildContext context, {bool botonAtras = true}) {
  return AppBar(
    title: GeneralImagen.logo,
    leading: (botonAtras)
        ? Navigator.canPop(context)
            ? IconButton(
                icon: const Icon(Icons.keyboard_arrow_left),
                onPressed: () => Navigator.of(context).pop(),
              )
            : null
        : null,
  );
}

Scaffold pantallaCargando() {
  return Scaffold(
    appBar: AppBar(title: GeneralImagen.logo, automaticallyImplyLeading: false),
    backgroundColor: Colors.grey.shade200,
    body: SafeArea(
      child: Container(
        constraints: const BoxConstraints.expand(),
        child: const Center(child: CircularProgressIndicator()),
      ),
    ),
  );
}

double obtenerMarco(Size pantalla) => pantalla.width * 0.06;
double obtenerAncho(Size pantalla) => pantalla.width * 0.94;
